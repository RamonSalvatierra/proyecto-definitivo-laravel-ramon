<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Crear Comanda') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
                    @if(session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif

                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif

                    <form action="{{ route('orders.store') }}" method="POST">
                        @csrf

                        <div class="mt-4">
                            <label for="client_id" class="block font-medium text-sm text-gray-700">{{ __('Cliente') }}</label>
                            <select name="client_id" id="client_id" class="block w-full mt-1">
                                @foreach ($clients as $client)
                                    <option value="{{ $client->id }}">{{ $client->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mt-4">
                            <label for="product_id" class="block font-medium text-sm text-gray-700">{{ __('Producto') }}</label>
                            <select name="product_id" id="product_id" class="block w-full mt-1">
                                @foreach ($products as $product)
                                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mt-4">
                            <label for="quantity" class="block font-medium text-sm text-gray-700">{{ __('Cantidad') }}</label>
                            <input type="number" name="quantity" id="quantity" class="block w-full mt-1" value="1" min="1" />
                        </div>

                        <div class="flex items-center justify-end mt-4">
                            <button class="ml-4">
                                Create order
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
