<!-- resources/views/reviews/createReviews.blade.php -->
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create Review') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="{{ route('reviews.storeReviews', $product->id) }}" method="POST">
                        @csrf

                        <div class="mt-4">
                            <label for="customer_id">Customer</label>
                            <select name="customer_id" id="customer_id" class="form-select mt-1 block w-full">
                                @foreach(App\Models\Customer::all() as $customer)
                                    <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mt-4">
                            <label for="rating">Rating</label>
                            <input type="number" name="rating" id="rating" min="1" max="5" class="form-input mt-1 block w-full">
                        </div>

                        <div class="mt-4">
                            <label for="comment">Comment</label>
                            <textarea name="comment" id="comment" class="form-textarea mt-1 block w-full"></textarea>
                        </div>

                        <div class="mt-4">
                            <button type="submit" class="bg-blue-500 text-white px-4 py-2 rounded">Add Review</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
