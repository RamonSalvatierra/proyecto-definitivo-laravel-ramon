<!-- resources/views/reviews/indexReviews.blade.php -->
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Reviews for {{ $product->name }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{{ route('reviews.createReviews', $product->id) }}" class="text-blue-500">Add Review</a>

                    @foreach($product->reviews as $review)
                        <div class="mt-4">
                            <strong>{{ $review->customer->name }}</strong>
                            <p>Rating: {{ $review->rating }}</p>
                            <p>{{ $review->comment }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
