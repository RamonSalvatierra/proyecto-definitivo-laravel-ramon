<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Confirm Delete') }}
        </h2>
    </x-slot>

    <div class="container mx-auto px-4 py-8">
        <h1 class="text-3xl font-semibold mb-4">Confirm Delete</h1>
        <p class="mb-4">Borrar de forma permanente el siguiente producto:  {{ $product->name }}?</p>
        <form action="{{ route('product.destroy', $product->id) }}" method="POST" class="mb-4">
            @csrf
            @method('DELETE')
            <button type="submit" class="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-4 rounded">Yes</button>
        </form>
        <a href="{{ route('products.indexProducts') }}" class="bg-gray-500 hover:bg-gray-600 text-white font-bold py-2 px-4 rounded">No</a>
    </div>
</x-app-layout>
