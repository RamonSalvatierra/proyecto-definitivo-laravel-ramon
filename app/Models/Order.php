<?php

namespace App\Models;

use App\Http\Controllers\ClientController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Order extends Model
{

    public $timestamps = false;

    public $fillable=['client_id', 'product_id', 'order_date', 'quantity', 'price_unity', 'total'];

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class)->withPivot('quantity', 'price_unity', 'total');
    }
}
