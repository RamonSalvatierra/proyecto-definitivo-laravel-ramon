<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;

class ClientController extends Controller
{
    public function index()
    {
        $clients = Client::all();
        return view('clients.indexClients', compact('clients'));
    }

    public function create()
    {
        return view('clients.createClients');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|unique:clients,email',
            'phone' => 'required|unique:clients,phone',
            'address' => 'required',
            'zip' => 'required|numeric',
        ]);

        $client = new Client();
        $client->name = $validatedData['name'];
        $client->email = $validatedData['email'];
        $client->phone = $validatedData['phone'];
        $client->address = $validatedData['address'];
        $client->zip = $validatedData['zip'];
        $client->save();

        return redirect()->route('clients.indexClients');
    }

    public function delete(Client $client)
    {
        $client->delete();
        return redirect()->route('clients.indexClients');
    }
}
