<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Client;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::all();
        return view('orders.indexOrders', compact('orders'));
    }

    public function create()
    {
        $clients = Client::all();
        $products = Product::all();
        return view('orders.createOrders', compact('clients', 'products'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'client_id' => 'required|exists:clients,id',
            'product_id' => 'required|exists:products,id',
            'order_date' => 'required|date',
            'products.*.quantity' => 'required|integer|min:1',
            'products.*.price' => 'required|numeric',
        ]);

        $product = Product::findOrFail($request->product_id);

        if ($product->quantity < $request->quantity) {
            return redirect()->route('orders.createOrders')->with('error', 'No hay suficientes existencias para este producto');
        }

        $order = new Order();
        $order->client_id = $request->client_id;
        $order->product_id = $request->product_id;
        $order->order_date = now();
        $order->quantity = $request->quantity;
        $order->price_unity = $product->price;
        $order->total = $request->quantity * $product->price;
        $order->save();

        $product->quantity -= $request->quantity;
        $product->save();

        return redirect()->route('orders.createOrders')->with('fino señores', 'Orden creada correctamente!');
    }
}
