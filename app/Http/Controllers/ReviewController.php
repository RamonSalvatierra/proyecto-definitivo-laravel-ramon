<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{



    public function index(Product $product)
    {
        $product->load('reviews.customer');
        return view('reviews.indexReviews', ['product'=>$product], compact('product'));
    }

    public function create(Product $product)
    {
        return view('reviews.createReviews', ['product'=>$product], compact('product'));
    }

    public function store(Request $request, Product $product)
    {
        $request->validate([
            'customer_id' => 'required|exists:customers,id',
            'rating' => 'required|integer|min:1|max:5',
            'comment' => 'nullable|string',
        ]);

        Review::create([
            'customer_id' => $request->customer_id,
            'product_id' => $product,
            'rating' => $request->rating,
            'comment' => $request->comment,
        ]);

        return redirect()->route('reviews.indexReviews', $product->id)->with('success', 'Review added successfully.');
    }
}
