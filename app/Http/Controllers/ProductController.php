<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;


class ProductController extends Controller
{
    public function indexProducts()
    {
        $products = Product::all();
        return view('products.indexProducts', compact('products'));
    }

    public function delete()
    {
        return view('products.confirmDeleteProducts');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.indexProducts');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'description' => 'nullable',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
        ]);

        $product = new Product();
        $product->name = $validatedData['name'];
        $product->description = $validatedData['description'];
        $product->quantity = $validatedData['quantity'];
        $product->price = $validatedData['price'];
        $product->save();

        return redirect()->route('products.indexProducts');
    }


}
