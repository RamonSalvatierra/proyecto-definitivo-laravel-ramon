CREATE DATABASE laravelProject;

DROP DATABASE IF EXISTS laravelProject;

DROP USER IF EXISTS 'ramon@localhost';

CREATE USER 'ramon@localhost' IDENTIFIED BY 'sigo';

GRANT ALL PRIVILEGES ON laravelProject.* TO 'ramon@localhost';
	
    
    
use laravelProject;
    
DROP TABLE IF EXISTS products;	

CREATE TABLE products (
	id INT auto_increment PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description TEXT,
    quantity INT,
    price DECIMAL(10, 2) NOT NULL
);


select * from products;

DROP TABLE IF EXISTS clients;	

CREATE TABLE clients (
	id INT auto_increment PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email varchar(255) not null,
    phone INT,
    address varchar(255),
    zip INT
);

select * from clients;

DROP TABLE IF EXISTS orders;	

CREATE TABLE orders (
    id INT PRIMARY KEY AUTO_INCREMENT,
    client_id INT,
    order_date DATETIME,
    FOREIGN KEY (client_id) REFERENCES clients(id),
    product_id INT,
    quantity INT,
    price_unity DECIMAL(10, 2),
    total DECIMAL(10, 2),
    FOREIGN KEY (product_id) REFERENCES products(id)
);


select * from orders;



DROP TABLE IF EXISTS reviews;	

CREATE TABLE reviews (
    id INT AUTO_INCREMENT PRIMARY KEY,
    product_id bigint unsigned NOT NULL,
    client_id BIGINT UNSIGNED NOT NULL,
    rating INT UNSIGNED DEFAULT 0,
    comment TEXT NULL,
    FOREIGN KEY (product_id) REFERENCES products(id),
    FOREIGN KEY (client_id) REFERENCES clients(id)
);

select * from reviews;






