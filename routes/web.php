<?php

use App\Http\Controllers\ClientController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ReviewController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;


Route::get('/', function () {
    return view('welcome');
});


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');


    /*productos*/

    Route::get('/products/indexProducts', [ProductController::class, 'indexProducts'])->name('products.indexProducts');
    Route::get('/product/{product}/delete', [ProductController::class, 'destroy'])->name('product.destroy');
    Route::get('/product/create', function () {
        return view('products.createProducts');
    })->name('products.createProducts');
    Route::post('/product/store', [ProductController::class, 'store'])->name('products.store');


    /*clientes*/

    Route::get('/clients', [ClientController::class, 'index'])->name('clients.indexClients');
    Route::get('/clients/{client}/delete', [ClientController::class, 'delete'])->name('clients.delete');
    Route::get('/clients/create', function () {
        return view('clients.createClients');
    })->name('clients.createClients');
    Route::post('/clients/store', [ClientController::class, 'store'])->name('clients.store');


    /*ordenes*/

    Route::get('/orders/indexOrders', [OrderController::class, 'index'])->name('orders.indexOrders');
    Route::get('/orders/create', [OrderController::class, 'create'])->name('orders.create');
    Route::post('/orders/store', [OrderController::class, 'store'])->name('orders.store');

    /*valoraciones*/

    Route::get('products/{product}/reviews', [ReviewController::class, 'index'])->name('reviews.indexReviews');
    Route::get('products/{product}/reviews/create', [ReviewController::class, 'create'])->name('reviews.createReviews');
    Route::post('products/{product}/reviews', [ReviewController::class, 'store'])->name('reviews.storeReviews');




});

require __DIR__.'/auth.php';
