<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('customers')->insert([
            ['name' => 'chayanne', 'age' => 130, 'email' => 'chayanne@gmail.com', 'phone' => 123456789, 'city' => 'Fuengirola'],
            ['name' => 'chayanne2', 'age' => 131, 'email' => 'chayanne2@gmail.com', 'phone' => 1234567890, 'city' => 'Fuentealbilla'],
            ['name' => 'chayanne3', 'age' => 132, 'email' => 'chayanne3@gmail.com', 'phone' => 1234567891, 'city' => 'Algeciras'],
        ]);
    }
}
