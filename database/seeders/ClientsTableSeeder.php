<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('clients')->insert([
            ['name' => 'Cliente 1', 'email' => 'cliente1@example.com', 'phone' => '123456789', 'address' => 'Dirección 1', 'zip' => 12345],
            ['name' => 'Cliente 2', 'email' => 'cliente2@example.com', 'phone' => '987654321', 'address' => 'Dirección 2', 'zip' => 54321],
            ['name' => 'Cliente 3', 'email' => 'cliente3@example.com', 'phone' => '456789123', 'address' => 'Dirección 3', 'zip' => 67890],
            ['name' => 'Cliente 4', 'email' => 'cliente4@example.com', 'phone' => '789123456', 'address' => 'Dirección 4', 'zip' => 98765],
            ['name' => 'Cliente 5', 'email' => 'cliente5@example.com', 'phone' => '321654987', 'address' => 'Dirección 5', 'zip' => 23456],
            ['name' => 'Cliente 6', 'email' => 'cliente6@example.com', 'phone' => '654987321', 'address' => 'Dirección 6', 'zip' => 87654],
        ]);
    }
}
