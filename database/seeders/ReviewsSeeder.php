<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ReviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $products = Product::pluck('id');
        $customers = Customer::pluck('id');

        $reviews = [
            [
                'product_id' => $products->random(),
                'customer_id' => $customers->random(),
                'rating' => 4,
                'comment' => 'Good product, highly recommended.',
            ],
            [
                'product_id' => $products->random(),
                'customer_id' => $customers->random(),
                'rating' => 5,
                'comment' => 'Excellent service and fast delivery!',
            ],
        ];

        Review::insert($reviews);
    }
}
