<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('products')->insert([
            ['name' => 'Producto 1', 'description' => 'Descripción del producto 1', 'quantity' => 1, 'price' => 10.00],
            ['name' => 'Producto 2', 'description' => 'Descripción del producto 2', 'quantity' => 2, 'price' => 20.00],
            ['name' => 'Producto 3', 'description' => 'Descripción del producto 3', 'quantity' => 3, 'price' => 30.00],
            ['name' => 'Producto 4', 'description' => 'Descripción del producto 4', 'quantity' => 4, 'price' => 40.00],
            ['name' => 'Producto 5', 'description' => 'Descripción del producto 5', 'quantity' => 5, 'price' => 50.00],
            ['name' => 'Producto 6', 'description' => 'Descripción del producto 6', 'quantity' => 6, 'price' => 60.00],
        ]);
    }
}
