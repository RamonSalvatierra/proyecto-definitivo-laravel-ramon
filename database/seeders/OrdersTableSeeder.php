<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('orders')->insert([
            ['client_id' => 1, 'product_id' => 1, 'order_date' => now(), 'quantity' => 7, 'total' => 70],
            ['client_id' => 1, 'product_id' => 2, 'order_date' => now(), 'quantity' => 2, 'total' => 40],
            ['client_id' => 2, 'product_id' => 1, 'order_date' => now(), 'quantity' => 7, 'total' => 70],
            ['client_id' => 2, 'product_id' => 2, 'order_date' => now(), 'quantity' => 5, 'total' => 100],
        ]);
    }
}
